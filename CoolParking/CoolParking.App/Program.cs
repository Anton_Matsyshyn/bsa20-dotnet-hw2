﻿using System;
using System.Collections.Generic;
using CoolParking.App.MenuClasses;
using CoolParking.BL.Services;

namespace CoolParking.App
{
    class Program
    {
        static void Main(string[] args)
        {
            ParkingService parkingService = new ParkingService(new TimerService(),
                                                               new TimerService(),
                                                               new LogService(@"D:\BSA2020\bsa20-dotnet-hw2\Transaction.log"));

            Menu menu = new Menu(new List<Item>
                                 {
                                    new Item("Vehicles",new List<Item>{
                                        new Item("Add new vehicle") ,
                                        new Item("Remove vehicle") ,
                                        new Item("Topup vehicle"),
                                        new Item("             ")
                                    }),
                                    new Item("Parking",new List<Item>{
                                        new Item("Parking balance"),
                                        new Item("Parking topup from last period"),
                                        new Item("Free places"),
                                        new Item("Parking transactions from last period"),
                                        new Item("All transactions"),
                                        new Item("All vehicles")
                                    })
                                 }, new ConsoleMenuService(parkingService, 
                                    new LogService(@"D:\BSA2020\bsa20-dotnet-hw2\Transaction.log"),
                                    new MessageService()));
            menu.Start();
        }
    }
}
