﻿using CoolParking.BL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CoolParking.App.MenuClasses
{
    public class Menu
    {
        readonly IConsoleMenuService _consoleService;
        public Menu(List<Item> items, IConsoleMenuService parkingService)
        {
            ItemsStack.Push(items);
            this._consoleService = parkingService;
        }
        public string Title { get; set; } = "Parking menu";
        public Stack<List<Item>> ItemsStack { get; private set; } = new Stack<List<Item>>();
        public Item SelectedItem { get; set; }
        public void Start()
        {
            SelectedItem = ItemsStack.Peek()[0];

            Console.CursorVisible = false;

            while (true)
            {
                var Items = ItemsStack.Peek();

                SelectedItem ??= Items[0];
                RefreshConsole();


                switch (Console.ReadKey().Key)
                {
                    case ConsoleKey.UpArrow:
                        {
                            int index = Items.IndexOf(SelectedItem);

                            if (index == 0)
                            {
                                SelectedItem = Items[^1];
                                break;
                            }

                            SelectedItem = Items[index - 1];

                            break;
                        }
                    case ConsoleKey.DownArrow:
                        {
                            int index = Items.IndexOf(SelectedItem);

                            if (index == Items.Count - 1)
                            {
                                SelectedItem = Items[0];
                                break;
                            }

                            SelectedItem = Items[index + 1];

                            break;
                        }
                    case ConsoleKey.Enter:
                        {
                            switch (SelectedItem.Name)
                            {
                                case "Vehicles":
                                    {
                                        ItemsStack.Push(SelectedItem.ChildItems);
                                        break;
                                    }
                                case "Add new vehicle":
                                    {
                                        _consoleService.AddNewVehicle();
                                        break;
                                    }
                                case "Remove vehicle":
                                    {
                                        _consoleService.RemoveVehicle();
                                        break;
                                    }
                                case "Topup vehicle":
                                    {
                                        _consoleService.TopupVehicle();
                                        break;
                                    }
                                case "Parking":
                                    {
                                        ItemsStack.Push(SelectedItem.ChildItems);
                                        break;
                                    }
                                case "Parking balance":
                                    {
                                        _consoleService.ParkingBalance();
                                        break;
                                    }
                                case "Parking topup from last period":
                                    {
                                        _consoleService.ParkingTopupFromLastPeriod();
                                        break;
                                    }
                                case "Free places":
                                    {
                                        _consoleService.FreePlaces();
                                        break;
                                    }
                                case "Parking transactions from last period":
                                    {
                                        _consoleService.ParkingTransactionFromLastPeriod();
                                        break;
                                    }
                                case "All transactions":
                                    {
                                        _consoleService.AllTransactions();
                                        break;
                                    }
                                case "All vehicles":
                                    {
                                        _consoleService.AllVehicles();
                                        break;
                                    }
                                case "             ":
                                    {
                                        _consoleService.EasterEgg();
                                        break;
                                    }
                            }

                            SelectedItem = null;

                            break;
                        }
                    case ConsoleKey.Backspace:
                        {
                            if (ItemsStack.Count == 1)
                            {
                                //Messages.Add(new Message { Value = "This is the last section, you cant go back.", MessageType = MessageType.Information });
                                break;
                            }
                            ItemsStack.Pop();
                            SelectedItem = ItemsStack.Peek()[0];
                            break;
                        }
                }
            }
        }
        public void RefreshConsole()
        {
            Console.Clear();

            WriteLine(Title);
            WriteLine("------------");
            Console.WriteLine();

            var Items = ItemsStack.Peek();

            foreach (var item in Items)
                if (item.Name != SelectedItem.Name)
                    WriteLine(item.Name);
                else
                {
                    Console.BackgroundColor = ConsoleColor.White;
                    Console.ForegroundColor = ConsoleColor.Black;
                    WriteLine(item.Name);
                    Console.ResetColor();
                }

            Console.WriteLine();
        }
        void WriteLine(string s)
        {
            Console.SetCursorPosition((Console.WindowWidth - s.Length) / 2, Console.CursorTop);
            Console.WriteLine(s);
        }
    }
}
