﻿// TODO: implement the ParkingService class from the IParkingService interface.
//       For try to add a vehicle on full parking InvalidOperationException should be thrown.
//       For try to remove vehicle with a negative balance (debt) InvalidOperationException should be thrown.
//       Other validation rules and constructor format went from tests.
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in ParkingServiceTests you can find the necessary constructor format and validation rules.
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text.RegularExpressions;
using System.Timers;

namespace CoolParking.BL.Services
{
    public class ParkingService : IParkingService
    {
        readonly ITimerService _withdrawTimer;
        readonly ITimerService _logTimer;
        readonly ILogService _logService;
        readonly Parking _parking;
        public ParkingService(ITimerService withdrawTimer,
                              ITimerService logTimer,
                              ILogService logService)
        {
            _withdrawTimer = withdrawTimer;
            _withdrawTimer.Interval = (double)Settings.CarPayPeriod*1000;
            _withdrawTimer.Elapsed += Withdraw_EventHandler;

            _logTimer = logTimer;
            _logTimer.Interval = (double)Settings.LoggingPeriod*1000;
            _logTimer.Elapsed += Logging_EventHandler;

            _logService = logService;
            _parking = Parking.GetInstance();

            _withdrawTimer.Start();
            _logTimer.Start();
        }
        public void AddVehicle(Vehicle vehicle)
        {
            if (_parking.Vehicles.Count == _parking.Vehicles.Capacity)
                throw new InvalidOperationException("There are no free places on the parking.");
            
            if (vehicle == null)
                new InvalidOperationException("Cannot add null vehicle.");

            if (!Vehicle.IsPlateNumberValid(vehicle.Id) || vehicle.Balance < 0)
                throw new ArgumentException("Cannot add vehicle with invalid parameters.");

            if (!IsVehicleIdUnique(vehicle.Id))
                throw new ArgumentException("Cannot add vehicle with non-unique id.");

            _parking.Vehicles.Add(vehicle);
        }

        public void Dispose()
        {
            _withdrawTimer.Dispose();
            _logTimer.Dispose();

            _parking.Balance = 0;
            _parking.Vehicles = new List<Vehicle>(Settings.ParkingCapacity);
            _parking.Transactions = new Queue<TransactionInfo>();
        }

        public decimal GetBalance()
        {
            return _parking.Balance;
        }

        public int GetCapacity()
        {
            return _parking.Vehicles.Capacity;
        }

        public int GetFreePlaces()
        {
            return _parking.Vehicles.Capacity - _parking.Vehicles.Count;
        }

        public TransactionInfo[] GetLastParkingTransactions()
        {
            return _parking.Transactions.ToArray();
        }

        public ReadOnlyCollection<Vehicle> GetVehicles()
        {
            return new ReadOnlyCollection<Vehicle>(_parking.Vehicles);
        }

        public string ReadFromLog()
        {
            return _logService.Read();
        }

        public void RemoveVehicle(string vehicleId)
        {
            var vehicle = _parking.Vehicles.FirstOrDefault(v => v.Id == vehicleId);

            if (vehicle == null)
                throw new ArgumentException();

            if (vehicle.Balance < 0)
                throw new InvalidOperationException();

            _parking.Vehicles.Remove(vehicle);
        }

        public void TopUpVehicle(string vehicleId, decimal sum)
        {
            if (sum < 0)
                throw new ArgumentException("Cannot topup negative sum.");

            var vehicle = _parking.Vehicles.FirstOrDefault(v => v.Id == vehicleId);

            if (vehicle == null)
                throw new ArgumentException($"Vehicle with id {vehicleId} does not exist.");

            vehicle.Balance += sum;
        }
        
        public void Withdraw_EventHandler(object sender, ElapsedEventArgs args)
        {
            foreach(var vehicle in _parking.Vehicles)
            {
                decimal transactionSum = 0;

                switch (vehicle.VehicleType)
                {
                    case VehicleType.PassengerCar:
                        {
                            transactionSum = vehicle.Balance - Settings.PassengerCarTariff >= 0 ?
                                             Settings.PassengerCarTariff :
                                             vehicle.Balance <= 0 ?
                                             Settings.PassengerCarTariff * Settings.DebtCoeff :
                                             vehicle.Balance + Math.Abs(vehicle.Balance - Settings.PassengerCarTariff) * Settings.DebtCoeff;

                            break;
                        }
                    case VehicleType.Truck:
                        {
                            transactionSum = vehicle.Balance - Settings.TruckTariff >= 0 ?
                                             Settings.TruckTariff :
                                             vehicle.Balance <= 0 ?
                                             Settings.TruckTariff * Settings.DebtCoeff :
                                             vehicle.Balance + Math.Abs(vehicle.Balance - Settings.TruckTariff) * Settings.DebtCoeff;

                            break;
                        }
                    case VehicleType.Bus:
                        {
                            transactionSum = vehicle.Balance - Settings.BusTariff >= 0 ?
                                             Settings.BusTariff :
                                             vehicle.Balance <= 0 ?
                                             Settings.BusTariff * Settings.DebtCoeff :
                                             vehicle.Balance + Math.Abs(vehicle.Balance - Settings.BusTariff) * Settings.DebtCoeff;

                            break;
                        }
                    case VehicleType.Motorcycle:
                        {
                            transactionSum = vehicle.Balance - Settings.MotorcycleTariff >= 0 ?
                                             Settings.MotorcycleTariff :
                                             vehicle.Balance <= 0 ?
                                             Settings.MotorcycleTariff * Settings.DebtCoeff :
                                             vehicle.Balance + Math.Abs(vehicle.Balance - Settings.MotorcycleTariff) * Settings.DebtCoeff;

                            break;
                        }
                }

                vehicle.Balance -= transactionSum;
                _parking.Balance += transactionSum;

                _parking.Transactions.Enqueue(new TransactionInfo(vehicle.Id, transactionSum));
            }
        }
        public void Logging_EventHandler(object sender, ElapsedEventArgs args)
        {
            string log = "";

            while (_parking.Transactions.Count != 0)
                log += $"Sum: {_parking.Transactions.Peek().Sum}\n" +
                       $"Time: {_parking.Transactions.Peek().Created}\n" +
                       $"Plate number: {_parking.Transactions.Dequeue().VehicleId}\n\n";

            _logService.Write(log);
        }
        private bool IsVehicleIdUnique(string vehicleId)
        {
            return !_parking.Vehicles.Select(v => v.Id).Contains(vehicleId);
        }
    }
}