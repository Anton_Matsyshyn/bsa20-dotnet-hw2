﻿using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CoolParking.BL.Services
{
    public class ConsoleMenuService : IConsoleMenuService
    {
        readonly IParkingService _parkingService;
        readonly ILogService _logService;
        readonly IMessageService _messageService;
        public ConsoleMenuService(IParkingService parkingService,
                                  ILogService logService,
                                  IMessageService messageService)
        {
            _parkingService = parkingService;
            _logService = logService;
            _messageService = messageService;
        }

        public void AddNewVehicle()
        {
            string vehicleId = "";
            int vehicleBalance = 0;
            VehicleType vehicleType;

            Console.CursorVisible = true;

            vehicleId = Validation(id => Vehicle.IsPlateNumberValid(id), "Plate number", "Example: FD-5432-YT");

            if (vehicleId == null)
                return;

            var balanceStr = Validation((b)=>int.TryParse(b,out int bInt) && bInt >=0,
                                        "Vehicle balance",
                                        "Vehicle balance must be non-negative");

            if (balanceStr == null)
                return;

            vehicleBalance = int.Parse(balanceStr);

            var vehicleTypeStr = Validation(t => int.TryParse(t, out int tInt) && tInt >= 0 && tInt <= 3,
                                            "Vehicle type",
                                            "Vehicle types:\n" +
                                            "0 - Passenger car\n" +
                                            "1 - Truck\n" +
                                            "2 - Bus\n" +
                                            "3 - Motorcycle\n");

            vehicleType = (VehicleType)int.Parse(vehicleTypeStr);

            Vehicle vehicle = null;

            try
            {
                vehicle = new Vehicle(vehicleId, vehicleType, vehicleBalance);
                _parkingService.AddVehicle(vehicle);

                Console.WriteLine("Congratulations! You add new vehicle to parking!");
            }
            catch(Exception ex)
            {
                _messageService.WriteError("Something went wrong :( We cannot create such vehicle.\n" +
                                           ex.Message);
            }

            Console.CursorVisible = false;

            Console.ReadKey();
        }

        public void AllTransactions()
        {
            Console.WriteLine(_logService.Read());
            Console.ReadKey();
        }

        public void AllVehicles()
        {
            Console.WriteLine("Here is all parking's vehicles");
            Console.WriteLine();

            Console.WriteLine("Plate num\t\tType\t\tBalance");
            Console.WriteLine();

            var vehicles = _parkingService.GetVehicles();

            foreach(var vehicle in vehicles)
                Console.WriteLine($"{vehicle.Id}\t\t{vehicle.VehicleType}\t\t{vehicle.Balance}");
            
            Console.ReadKey();
        }

        public void FreePlaces()
        {
            Console.WriteLine($"For now there are {_parkingService.GetFreePlaces()} free places");
            Console.ReadKey();
        }

        public void ParkingBalance()
        {
            Console.WriteLine($"For now parking balance is {_parkingService.GetBalance()}");
            Console.ReadKey();
        }

        public void ParkingTopupFromLastPeriod()
        {
            var sum = _parkingService.GetLastParkingTransactions().Select(t => t.Sum).Sum();
            
            Console.WriteLine($"Transaction topup from last period: {sum}");

            Console.ReadKey();
        }

        public void ParkingTransactionFromLastPeriod()
        {
            var transactions = _parkingService.GetLastParkingTransactions();
            foreach(var t in transactions)
                Console.WriteLine($"Sum: {t.Sum}\n" +
                                  $"Time: {t.Created}\n" +
                                  $"Plate number: {t.VehicleId}\n");

            Console.ReadKey();
        }

        public void RemoveVehicle()
        {
            Console.CursorVisible = true;

            Console.WriteLine("This section helps you to remove vehicle from parking.");

            var vehicleId = Validation(id => Vehicle.IsPlateNumberValid(id),
                                      "vehicle plate number",
                                      "Input id of vehicle, which you want to remove");

            var vehicle = _parkingService.GetVehicles().FirstOrDefault(v => v.Id == vehicleId);

            if(vehicle==null)
            {
                _messageService.WriteWarning("Vehicle with such plate number does not exist.");
                Console.ReadKey();
                return;
            }

            Console.WriteLine("Plate num\t\tType\t\tBalance");
            Console.WriteLine($"{vehicle.Id}\t\t{vehicle.VehicleType}\t\t{vehicle.Balance}");
            Console.WriteLine();

            Console.WriteLine("You want to remove this vehicle? (Y/N)");
            var ans = Console.ReadLine().ToLower()[0];

            if (ans != 'y')
                return;

            try
            {
                _parkingService.RemoveVehicle(vehicleId);

                Console.WriteLine();
                _messageService.WriteInformation($"Vehicle with id {vehicleId} was successful removed");
            }
            catch(Exception ex)
            {
                _messageService.WriteError(ex.Message);
            }

            Console.CursorVisible = false;

            Console.ReadKey();
        }

        public void TopupVehicle()
        {
            Console.CursorVisible = true;

            Console.WriteLine("This section helps you to topup vehicle.");

            var vehicleId = Validation(id => Vehicle.IsPlateNumberValid(id),
                                      "vehicle plate number",
                                      "Input id of vehicle, which you want to topup");

            var vehicle = _parkingService.GetVehicles().FirstOrDefault(v => v.Id == vehicleId);

            if (vehicle == null)
            {
                _messageService.WriteWarning("Vehicle with such plate number does not exist.");
                Console.ReadKey();
                return;
            }

            var topupSumStr = Validation(t => int.TryParse(t, out _),
                                        "topup sum",
                                        "Enter topup sum");

            if (topupSumStr == null)
                return;

            var topupSum = int.Parse(topupSumStr);

            try
            {
                _parkingService.TopUpVehicle(vehicleId, topupSum);
                _messageService.WriteInformation($"Vehicle {vehicleId} was successful topuped");
            }
            catch(Exception ex)
            {
                _messageService.WriteError(ex.Message);
            }

            Console.CursorVisible = false;

            Console.ReadKey();
        }
        public void EasterEgg()
        {
            Console.WriteLine("О боже, дякую що ви зайшли сюди!");
            Console.WriteLine();
            Console.ReadKey();

            Console.WriteLine("Розробники викрали мене i заховали у цiй програмi!..");
            Console.WriteLine();
            Console.ReadKey();

            Console.WriteLine("Для чого? А самi не розумiєте?");
            Console.WriteLine();
            Console.ReadKey();

            Console.WriteLine("Хто ж iще буде виставляти цi бiли лiтери на чорному фонi?");
            Console.WriteLine();
            Console.ReadKey();

            Console.WriteLine("Що-що? Якi команди, якi байти i бiти, це все роблю я.");
            Console.WriteLine();
            Console.ReadKey();

            Console.WriteLine("Дiдько! Сюди прямує збiрщик смiття! Менi граблi...");
            Console.WriteLine();
            Console.ReadKey();

            Console.WriteLine("I пам'ятайте: реальнiсть - iлюзiя");
            Console.WriteLine();
            Console.ReadKey();

            Console.WriteLine("Усесвiт - голограма");
            Console.WriteLine();
            Console.ReadKey();

            Console.WriteLine("Скупайте золото");
            Console.WriteLine();
            Console.ReadKey();

            Console.WriteLine("Па-па");
            Console.WriteLine();
            Console.ReadKey();

        }
        private string Validation(Func<string,bool> validation, string paramName, string description)
        {
            do
            {
                Console.WriteLine("Input correct "+ paramName);
                Console.WriteLine(description);

                var str = Console.ReadLine();

                if (validation(str))
                {
                    Console.WriteLine();
                    _messageService.WriteInformation(paramName + " is correct.");
                    Console.WriteLine();

                    return str;
                }
                else
                {
                    Console.WriteLine();
                    _messageService.WriteError(paramName + " is not valid. Try one more time? (Y/N)");
                    Console.WriteLine();

                    if (Console.ReadLine().ToLower()[0] != 'y')
                    {
                        Console.WriteLine("Tap any key to go to the menu.");
                        Console.ReadKey();

                        return null;
                    }
                }

            } while (true);
        }
    }
}
