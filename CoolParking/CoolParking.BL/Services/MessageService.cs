﻿using CoolParking.BL.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace CoolParking.BL.Services
{
    public class MessageService : IMessageService
    {
        public void WriteError(string mes)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(mes);
            Console.ResetColor();
        }

        public void WriteInformation(string mes)
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine(mes);
            Console.ResetColor();
        }

        public void WriteWarning(string mes)
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine(mes);
            Console.ResetColor();
        }
    }
}
