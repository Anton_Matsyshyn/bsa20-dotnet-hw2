﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CoolParking.BL.Interfaces
{
    public interface IConsoleMenuService
    {
        void AddNewVehicle();
        void RemoveVehicle();
        void TopupVehicle();
        void ParkingBalance();
        void ParkingTopupFromLastPeriod();
        void FreePlaces();
        void ParkingTransactionFromLastPeriod();
        void AllTransactions();
        void AllVehicles();
        void EasterEgg();
    }
}
