﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CoolParking.BL.Interfaces
{
    public interface IMessageService
    {
        void WriteInformation(string mes);
        void WriteWarning(string mes);
        void WriteError(string mes);
    }
}
