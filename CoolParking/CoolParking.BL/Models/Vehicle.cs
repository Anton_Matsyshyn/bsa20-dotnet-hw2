﻿// TODO: implement class Vehicle.
//       Properties: Id (string), VehicleType (VehicleType), Balance (decimal).
//       The format of the identifier is explained in the description of the home task.
//       Id and VehicleType should not be able for changing.
//       The Balance should be able to change only in the CoolParking.BL project.
//       The type of constructor is shown in the tests and the constructor should have a validation, which also is clear from the tests.
//       Static method GenerateRandomRegistrationPlateNumber should return a randomly generated unique identifier.
using System;
using System.Text.RegularExpressions;

namespace CoolParking.BL.Models
{
    public class Vehicle
    {
        public Vehicle(string Id, VehicleType vehicleType, decimal Balance)
        {
            this.Id = IsPlateNumberValid(Id) ? Id : throw new ArgumentException();
            VehicleType = vehicleType;
            this.Balance = Balance >= 0 ? Balance : throw new ArgumentException();
        }
        public string Id { get; }
        public VehicleType VehicleType { get; }
        public decimal Balance { get; internal set; }
        public static bool IsPlateNumberValid(string plateNumber)
        {
            try
            {
                Regex regex = new Regex("^[A-Z]{2}-[0-9]{4}-[A-Z]{2}$");
                return regex.IsMatch(plateNumber);
            }
            catch (Exception ex) { }

            return false;
        }
        public static string GenerateRandomRegistrationPlateNumber()
        {
            string plateNumber = "AA-1000-AA";

            int indexOfA = 65;
            int indexOfZ = 90;

            try
            {
                Random random = new Random();

                plateNumber = "" + (char)random.Next(indexOfA, indexOfZ + 1)
                              + (char)random.Next(indexOfA, indexOfZ + 1)
                              + '-'
                              + random.Next(1000, 9999)
                              + '-'
                              + (char)random.Next(indexOfA, indexOfZ + 1)
                              + (char)random.Next(indexOfA, indexOfZ + 1);
            }
            //Я хотів би додати сюди побільше обробників, але просто не знаю, що може підти не так.
            catch (Exception ex) { }

            return plateNumber;
        }
    }
}