﻿// TODO: implement class Settings.
//       Implementation details are up to you, they just have to meet the requirements of the home task.
namespace CoolParking.BL.Models
{
    public static class Settings
    {
        public static decimal ParkingStartBalance { get; private set; } = 0;
        public static int ParkingCapacity { get; private set; } = 10;
        public static decimal CarPayPeriod { get; private set; } = 5M;
        public static decimal LoggingPeriod { get; private set; } = 60M;
        public static decimal DebtCoeff { get; private set; } = 2.5M;
        //Tariffs
        public static decimal PassengerCarTariff { get; private set; } = 2M;
        public static decimal TruckTariff { get; private set; } = 5M;
        public static decimal BusTariff { get; private set; } = 3.5M;
        public static decimal MotorcycleTariff { get; private set; } = 1M;
    }
}