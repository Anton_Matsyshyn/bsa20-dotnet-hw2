﻿// TODO: implement struct TransactionInfo.
//       Necessarily implement the Sum property (decimal) - is used in tests.
//       Other implementation details are up to you, they just have to meet the requirements of the homework.
using System;

namespace CoolParking.BL.Models
{
    public struct TransactionInfo
    {
        public  TransactionInfo(string VehicleId, decimal Sum)
        {
            this.VehicleId = VehicleId;
            this.Sum = Sum;

            Created = DateTime.Now;
        }
        public DateTime Created { get; private set; }
        public string VehicleId { get; private set; }
        public decimal Sum { get; private set; }
    }
}